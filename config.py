from os import getenv

SQLALCHEMY_DATABASE_URI = getenv(
    "SQLALCHEMY_DATABASE_URI",
    "postgresql+psycopg2://username:passwd!@localhost:5432/blog",
)


class Config:
    TESTING = False
    DEBUG = False
    SECRET_KEY = "qwertytrewsupersecret"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = SQLALCHEMY_DATABASE_URI
    WTF_CSRF_ENABLED = True


class ProductionConfig(Config):
    ENV = "production"
    SECRET_KEY = "prod_qwertytrewsupersecret"


class DevelopmentConfig(Config):
    ENV = "development"
    DEBUG = True
    SECRET_KEY = "dev_qwertytrewsupersecret"


class TestingConfig(Config):
    ENV = "testing"
    TESTING = True
    WTF_CSRF_ENABLED = False
    SERVER_NAME = "server.testing"
