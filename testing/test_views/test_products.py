from http import HTTPStatus
from time import time

from flask import url_for

from models import Product


def test_add_product(client):
    product_name = f"product_{time()}"
    is_new = True

    product = Product.query.filter_by(name=product_name).one_or_none()
    assert product is None

    data = {
        "product-name": product_name,
        "is-new": is_new,
    }
    url = url_for("products_app.add")
    response = client.post(url, data=data)

    assert response.status_code < HTTPStatus.BAD_REQUEST, response.text
    assert 'location' in response.headers

    product: Product = Product.query.filter_by(name=product_name).one_or_none()
    assert product is not None
    assert product.is_new is is_new


class TestGetProduct:
    def test_product_not_found(self, client):
        url = url_for("products_app.details", product_id=0)
        response = client.get(url)
        assert response.status_code == HTTPStatus.NOT_FOUND, response.text
